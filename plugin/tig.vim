vim9script
# Plugin:  vim-tig  
# Description: Open command line tig https://jonas.github.io/tig/ in a popup
#              dialog.
# Maintainer:  S. Tessarin https://tessarinseve.pythonanywhere.com/nws/index.html

# tig executable default path
if !exists('g:vimtig_tig_exe')
    var g:vimtig_tig_exe = "tig.exe" 
endif

if argc() != 0 || v:version < 900
    finish
else
    augroup vimtig
      autocmd VimEnter    *   call On_vimenter()
    augroup END
endif 

# close popup with esc
def Close_Tig(winid: number, key: string): bool
    if key == '\<ESC>'
        call popup_close(winid)
    endif

    return popup_filter_menu(winid, key)
enddef

def On_vimenter()
    var cwd =  getcwd()
    var check_tig_exe = system(g:vimtig_tig_exe .. " -v")
    var cmdline = g:vimtig_tig_exe .. ' -C ' .. cwd
    if v:shell_error == 0

        #call append('$', [cmdline])
        var bufnr = term_start(cmdline, {'hidden': 1})
        var winid = popup_create(bufnr,
                        \ {
                        \ 'minwidth': 100,
                        \ 'maxwidth': 115,
                        \ 'minheight': 28,
                        \ 'maxheight': 28, 
                        \ "close": "button",
                        \ "filter": function('Close_Tig'),
                        \ 'border': [],
                        \ 'borderchars': ['─', '│', '─', '│', '┌', '┐', '┘', '└']
                        \    })
    else 
        popup_notification(check_tig_exe, {'pos': 'topright'})   
        finish
    endif 

enddef

